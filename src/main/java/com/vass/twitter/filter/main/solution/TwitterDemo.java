package com.vass.twitter.filter.main.solution;

import static spark.Spark.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Function;

import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;

public class TwitterDemo {
	
	

	public static void main(String[] args)  {
		
		//Lista de Tweets con la cadena elegida
		get("/tweets/:parametro", (req,res)->{
			
			Function<String, List<String>> resultTweets = querySearch -> {
				
				List<String> listaRetorno=new ArrayList<String>();
				Twitter twitter = TwitterFactory.getSingleton();
				Query query = new Query(querySearch);
				try {
					QueryResult result = twitter.search(query);
					result.getTweets().forEach(status->listaRetorno.add("@" + status.getUser().getScreenName() + ":" + status.getText()));
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				return listaRetorno;
			};
		
			return resultTweets.apply(req.params(":parametro"));
		});
		
		
		//Fecha del �ltimo tweet con la palabra elegida
		get("/fecha/:parametro", (req,res)->{
			
			Function<String, Date> tweetDate = (cadena)->{
				Twitter twitter = TwitterFactory.getSingleton();
				Query query = new Query(cadena);
				QueryResult result=null;
				try {
					result = twitter.search(query);
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
				return result.getTweets().get(0).getCreatedAt();
			};
			
			return tweetDate.apply(req.params(":parametro"));
		});
		
		//Autor de los tweets
		get("/autor/:parametro", (req,res)->{
			
			Function<String,String> tweetAuthor = (cadena)->{
				Twitter twitter = TwitterFactory.getSingleton();
				Query query = new Query(cadena);
				QueryResult result=null;
				try {
					result = twitter.search(query);
					 
				} catch (Exception e) {
					e.printStackTrace();
				}
				return result.getTweets().get(0).getUser().getName();
			};
			return tweetAuthor.apply(req.params(":parametro"));
		});
		
		
		
	}
	

}
